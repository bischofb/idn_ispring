﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SqlToolsConfigString;
using System.Xml;
using System.Text.RegularExpressions;
using System.Data;
using NLog;
using System.IO;
using Newtonsoft.Json;

namespace iSpring
{
    public partial class _Default : System.Web.UI.Page
    {
        Logger logger;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //SaveQuiz(3, 3, 715016, 1);
                Library.NLoggerSetup(ref logger);
                logger.Debug("Site page loading");
                logger.Debug("Form keys on page: {0}", Request.Form.Keys.Count);
                if (Request.Form.Keys.Count > 0)
                {
                    try
                    {
                        int personId = GetPersonId();

                        int sp = int.Parse("0" + Request.Form["sp"]);
                        logger.Debug("Num Correct: {0}", sp.ToString());
                        int tp = int.Parse("0" + Request.Form["tp"]);
                        logger.Debug("Total Q's: {0}", tp.ToString());

                        string quizField = Request.Form["qt"];
                        logger.Debug("Quiz Field: {0}", quizField.ToString());

                        var quizFieldSplit = quizField.Split('_');
                        int quizId = 0;
                        if (quizFieldSplit.Length > 0)
                        {
                            if (int.TryParse(quizFieldSplit[quizFieldSplit.Length - 1], out quizId))
                            {
                                SaveQuiz(tp, sp, personId, quizId);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        logger.Debug("Error: {0}", ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Debug("Page Load: {0}", ex.Message);
            }
        }

        private void SaveQuiz(int NumberQuestions, int NumberCorrect, int PersonId, int QuizId)
        {
            int scheduleId = 0, courseId = 0, registrationId = 0;
            string courseTitle = "", systemId = "", courseIdxx = "";
            int score = (int)Math.Truncate((double)NumberCorrect * 100 / (double)NumberQuestions);
//#if !DEBUG
            systemId = Request.ServerVariables["HTTP_SYSTEMID"];
//#else
//            systemId = "2c9034e7271a58fa01273fd7395b0816";
//#endif
            GetCourseInfo(PersonId, QuizId, ref courseId, ref courseIdxx, ref courseTitle, ref registrationId, ref scheduleId);
            SaveQuizRecord(QuizId, courseId, registrationId, score);
            if (SaveCourseCompletion(PersonId, courseId, registrationId))
            {
                IACUCWebService(registrationId, systemId, courseIdxx, courseTitle, scheduleId);
            }
        }

        private int GetPersonId()
        {
            Library.NLoggerSetup(ref logger);

            int personId = 0;
            var systemId = Request.ServerVariables["HTTP_SYSTEMID"];
            logger.Debug("System ID: {0}", (systemId ?? ""));
            //systemId = "2c9034e7271a58fa01273fd7395b0816";
            if (systemId != null)
            {
                SqlBuilder sql = new SqlBuilder();
                sql.Append("Select person_id from phoenix.system_users");
                sql.Append("WHERE system_id='{0}'", systemId);
                int.TryParse(SqlHelper.ExecuteScalar(Library.ConnectionString(), sql.ToString()).ToString(), out personId);
            }
            return personId;
        }

        private void GetCourseInfo(int PersonId, int QuizId, ref int CourseId, ref string CourseIdxx, ref string CourseTitle, ref int RegistrationId, ref int ScheduleId)
        {
            SqlBuilder sql = new SqlBuilder();
//#if DEBUG
//            PersonId = 105;
//#endif
            sql.Append("select * from ");
            sql.Append("phoenix.training_quizzes q ");
            sql.Append("inner join tblTrainingCourses c on c.CourseId=q.training_course_id");
            sql.Append("inner join tblTrainingRegistration r on r.CourseId=c.CourseId");
            sql.Append("inner join tblTrainingSchedule s on s.ScheduleId=r.ScheduleId");
            sql.Append("where q.id={0} and PersonId={1} and SessionClosed=0 ORDER BY StartDateTime DESC", QuizId, PersonId);
            DataTable dt = SqlHelper.ExecuteDatatable(Library.ConnectionString(), sql.ToString());

            if (dt.Rows.Count > 0)
            {
                RegistrationId = int.Parse(dt.Rows[0]["TrainingRegistrationId"].ToString());
                ScheduleId = int.Parse(dt.Rows[0]["ScheduleId"].ToString());
                CourseId = int.Parse(dt.Rows[0]["CourseId"].ToString());
                CourseIdxx = dt.Rows[0]["CourseIdxx"].ToString();
                CourseTitle = dt.Rows[0]["CourseTitle"].ToString();
            }
        }

        private void SaveQuizRecord(int QuizId, int courseId, int RegistrationId, int Score)
        {
            Library.NLoggerSetup(ref logger);
            string exists = "";
            SqlBuilder sql = new SqlBuilder();
            try
            {
                sql.Append("SELECT 1 FROM phoenix.training_quiz_scores");
                sql.Append("WHERE training_quiz_id={0} AND training_registration_id={1}", QuizId, RegistrationId);
                exists = SqlHelper.ExecuteScalar(Library.ConnectionString(), sql.ToString()).ToString();
            }
            catch
            {
                sql = new SqlBuilder();
                sql.Append("INSERT INTO phoenix.training_quiz_scores");
                sql.Append("(training_registration_id, training_quiz_id, score,");
                sql.Append("completed_at, created_at, updated_at)");
                sql.Append("Values({0},{1},{2}, getdate(), getdate(), getdate())", RegistrationId, QuizId, Score);
                try
                {
                    SqlHelper.ExecuteNonQuery(Library.ConnectionString(), sql.ToString());
                    logger.Debug("successfully inserted");
                }
                catch (Exception ex)
                {
                    logger.Debug("Insert Error - QuizId:{0}; {2}", QuizId, ex.Message);
                }
            }
            if (exists != "")
            {
                try
                {
                    sql = new SqlBuilder();
                    sql.Append("Update phoenix.training_quiz_scores");
                    sql.Append("SET Score={0}", Score);
                    sql.Append("WHERE training_registration_id={0} AND training_quiz_id={1}", RegistrationId, QuizId);
                    SqlHelper.ExecuteNonQuery(Library.ConnectionString(), sql.ToString()).ToString();
                    logger.Debug("Successfully updated");
                }
                catch (Exception ex)
                {
                    logger.Debug("Update Error - QuizId:{0}; {1}", QuizId, ex.Message);
                }

            }
        }

        private bool SaveCourseCompletion(int PersonId, int CourseId, int RegistrationId)
        {
            bool result = false;
            Library.NLoggerSetup(ref logger);
            SqlBuilder sql = new SqlBuilder();
            sql.Append("select count(*) from");
            sql.Append("phoenix.training_quiz_scores s");
            sql.Append("inner join tblTrainingRegistration r on r.TrainingRegistrationId=s.training_registration_id");
            sql.Append("where TrainingRegistrationId={0} and PersonID={1}", RegistrationId, PersonId);
            int quizzesTaken = int.Parse(SqlHelper.ExecuteScalar(Library.ConnectionString(), sql.ToString()).ToString());

            sql = new SqlBuilder();
            sql.Append("select count(*) from phoenix.training_quizzes where training_course_id={0}", CourseId);  
            int totalQuizzes = int.Parse(SqlHelper.ExecuteScalar(Library.ConnectionString(), sql.ToString()).ToString());

            if (totalQuizzes != 0 && totalQuizzes == quizzesTaken)
            {
                try
                {
                    logger.Debug("Saving Course Completion record  - PersonId:{0}; CourseId: {1}", PersonId, CourseId);
                    sql = new SqlBuilder();
                    sql.Append("UPDATE tblTrainingRegistration Set Attended=1 WHERE TrainingRegistrationId={0}", RegistrationId);
                    SqlHelper.ExecuteNonQuery(Library.ConnectionString(), sql.ToString());
                    result = true;
                    logger.Debug("Success! Course Completion record  - PersonId:{0}; CourseId: {1}", PersonId, CourseId);
                }
                catch (Exception ex)
                {
                    logger.Debug("Update Error - PersonId:{0}; CourseId: {1}; {2}", PersonId, CourseId, ex.Message);
                }
            }
            return result;
        }


        private void IACUCWebService(int RemoteRecId, string SystemId, string CourseIdxx, string CourseTitle, int ScheduleId){
        string targetUri = "";
        Library.NLoggerSetup(ref logger);
//#if !DEBUG
    targetUri = @"http://aups.ucsd.edu/post/training";
//#else
  //      targetUri = @"http://aups-dev.ucsd.edu/post/training";
//#endif
            StringWriter sw = new StringWriter();
            string sendDate = String.Format("{0:yyyy-MM-dd HH:mm:ss}", DateTime.Now);
            string JsonResult = "";
            bool attended = true;
            try
            {
                JsonWriter jsonWriter;
                jsonWriter = new JsonWriter(sw);
                jsonWriter.WriteStartObject();
                jsonWriter.WritePropertyName("request_type");
                jsonWriter.WriteValue("training");
                jsonWriter.WritePropertyName("action");
                jsonWriter.WriteValue("add_or_update");
                jsonWriter.WritePropertyName("sent_at");
                jsonWriter.WriteValue(sendDate);
                //start new data object
                jsonWriter.WritePropertyName("data_object");
                jsonWriter.WriteStartObject();
                jsonWriter.WritePropertyName("system_id");
                jsonWriter.WriteValue(SystemId);
                jsonWriter.WritePropertyName("course_id");
                jsonWriter.WriteValue(CourseIdxx);
                jsonWriter.WritePropertyName("course_title");
                jsonWriter.WriteValue(CourseTitle);
                jsonWriter.WritePropertyName("schedule_id");
                jsonWriter.WriteValue(ScheduleId);
                jsonWriter.WritePropertyName("datetime");
                jsonWriter.WriteValue(sendDate);
                jsonWriter.WritePropertyName("attended");
                jsonWriter.WriteValue(attended);
                jsonWriter.WriteEndObject();
                jsonWriter.WriteEndObject();

                JsonResult = sw.ToString();
                sw.Dispose();

                int ApplicationId, EntityId;
                SqlBuilder sql;

                sql = new SqlBuilder();
                sql.Append("SELECT id FROM phoenix.system_environments WHERE name='Intranet'");
                EntityId = int.Parse(SqlHelper.ExecuteScalar(Library.ConnectionString(), sql.ToString()).ToString());

                sql = new SqlBuilder();
                sql.Append("SELECT id FROM phoenix.system_applications WHERE name='iSpring Quiz Processor' AND system_environment_id={0}", EntityId);
                ApplicationId = int.Parse(SqlHelper.ExecuteScalar(Library.ConnectionString(), sql.ToString()).ToString());

                sql = new SqlBuilder();
                sql.Append("INSERT INTO phoenix.web_service_queue");
                sql.Append("(system_application_id, source_table, source_column, key_value, status,");
                sql.Append("target_url, json_request, submitted_at)");
                sql.Append("VALUES ({0}, 'tblTrainingRegistration', 'TrainingRegistrationID', {1},", ApplicationId, RemoteRecId);
                sql.Append("'pending', '{0}', '{1}', GetDate())", targetUri, JsonResult);
                SqlHelper.ExecuteNonQuery(Library.ConnectionString(), sql.ToString());
                logger.Debug("Record written to web service {0}", SystemId);
            }
            catch (Exception Ex)
            {
                if (SystemId != "1000046565")  //this is my user id
                {
                    logger.Error("Web Service Error - SystemId: {0}, {1}", SystemId, Ex.Message);
                }
            }
        }

    }
}

