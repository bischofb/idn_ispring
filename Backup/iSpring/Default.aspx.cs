﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SqlToolsConfigString;
using System.Xml;
using System.Text.RegularExpressions;
using System.Data;
using NLog;

namespace iSpring
{
    public partial class _Default : System.Web.UI.Page
    {
        Logger logger;

        protected void Page_Load(object sender, EventArgs e)
        {
            Library.NLoggerSetup(ref logger);
            logger.Debug("Site page loading");
            logger.Debug("Form keys on page: {0}", Request.Form.Keys.Count);
            if (Request.Form.Keys.Count > 0)
            {
                try
                {
                    int personId = GetPersonId();

                    int sp = int.Parse("0" + Request.Form["sp"]);
                    logger.Debug("Num Correct: {0}", sp.ToString());
                    int tp = int.Parse("0" + Request.Form["tp"]);
                    logger.Debug("Total Q's: {0}", tp.ToString());

                    string quizField = Request.Form["qt"];
                    logger.Debug("Quiz Field: {0}", quizField.ToString());

                    var quizFieldSplit = quizField.Split('_');
                    int quizId = 0;
                    if (quizFieldSplit.Length > 0)
                    {
                        if (int.TryParse(quizFieldSplit[quizFieldSplit.Length - 1], out quizId))
                        {
                            SaveQuiz(tp, sp, personId, quizId);
                        }
                    }
                }
                catch (Exception ex)
                {
                    logger.Debug("Error: {0}", ex.Message);
                }
            }
        }

        private void SaveQuiz(int NumberQuestions, int NumberCorrect, int PersonId, int QuizId)
        {
            int scheduleId = 0, courseId = 0, registrationId = 0;
            int score = (int)Math.Truncate((double)NumberCorrect * 100 / (double)NumberQuestions);
            GetCourseInfo(PersonId, QuizId, ref courseId, ref registrationId, ref scheduleId);
            SaveQuizRecord(QuizId, courseId, registrationId, score);
            SaveCourseCompletion(PersonId, courseId, registrationId);
        }

        private int GetPersonId()
        {

            int personId = 0;
            var systemId = Request.ServerVariables["HTTP_SYSTEMID"];
            logger.Debug("System ID: {0}", (systemId ?? ""));
            //systemId = "2c9034e7271a58fa01273fd7395b0816";
            if (systemId != null)
            {
                SqlBuilder sql = new SqlBuilder();
                sql.Append("Select person_id from phoenix.system_users");
                sql.Append("WHERE system_id='{0}'", systemId);
                int.TryParse(SqlHelper.ExecuteScalar(Library.ConnectionString(), sql.ToString()).ToString(), out personId);
            }
            return personId;
        }

        private void GetCourseInfo(int PersonId, int QuizId, ref int CourseId, ref int RegistrationId, ref int ScheduleId)
        {
            SqlBuilder sql = new SqlBuilder();

            sql.Append("select * from ");
            sql.Append("phoenix.training_quizzes q ");
            sql.Append("inner join tblTrainingCourses c on c.CourseId=q.training_course_id");
            sql.Append("inner join tblTrainingRegistration r on r.CourseId=c.CourseId");
            sql.Append("inner join tblTrainingSchedule s on s.ScheduleId=r.ScheduleId");
            sql.Append("where q.id={0} and PersonId={1}", QuizId, PersonId);
            DataTable dt = SqlHelper.ExecuteDatatable(Library.ConnectionString(), sql.ToString());

            if (dt.Rows.Count > 0)
            {
                RegistrationId = int.Parse(dt.Rows[0]["TrainingRegistrationId"].ToString());
                ScheduleId = int.Parse(dt.Rows[0]["ScheduleId"].ToString());
                CourseId = int.Parse(dt.Rows[0]["CourseId"].ToString());
            }
        }

        private void SaveQuizRecord(int QuizId, int courseId, int RegistrationId, int Score)
        {
            string exists = "";
            SqlBuilder sql = new SqlBuilder();
            try
            {
                sql.Append("SELECT 1 FROM phoenix.training_quiz_scores");
                sql.Append("WHERE training_quiz_id={0} AND training_registration_id={1}", QuizId, RegistrationId);
                exists = SqlHelper.ExecuteScalar(Library.ConnectionString(), sql.ToString()).ToString();
            }
            catch
            {
                sql = new SqlBuilder();
                sql.Append("INSERT INTO phoenix.training_quiz_scores");
                sql.Append("(training_registration_id, training_quiz_id, score,");
                sql.Append("completed_at, created_at, updated_at)");
                sql.Append("Values({0},{1},{2}, getdate(), getdate(), getdate())", RegistrationId, QuizId, Score);
                try
                {
                    SqlHelper.ExecuteNonQuery(Library.ConnectionString(), sql.ToString());
                    logger.Debug("successfully inserted");
                }
                catch (Exception ex)
                {
                    logger.Debug("Insert Error - QuizId:{0}; {2}", QuizId, ex.Message);
                }
            }
            if (exists != "")
            {
                try
                {
                    sql.Append("Update  phoenix.training_quiz_scores");
                    sql.Append("SET Score={0}", Score);
                    sql.Append("WHERE training_registration_id={0} AND training_quiz_id={1}", RegistrationId, QuizId);
                    SqlHelper.ExecuteNonQuery(Library.ConnectionString(), sql.ToString()).ToString();
                    logger.Debug("Successfully updated");
                }
                catch (Exception ex)
                {
                    logger.Debug("Update Error - QuizId:{0}; {2}", QuizId, ex.Message);
                }

            }
        }

        private void SaveCourseCompletion(int PersonId, int CourseId, int RegistrationId)
        {
            SqlBuilder sql = new SqlBuilder();
            sql.Append("select count(*) from");
            sql.Append("phoenix.training_quiz_scores s");
            sql.Append("inner join tblTrainingRegistration r on r.TrainingRegistrationId=s.training_registration_id");
            sql.Append("where TrainingRegistrationId={0} and PersonID={1}", RegistrationId, PersonId);
            int quizzesTaken = int.Parse(SqlHelper.ExecuteScalar(Library.ConnectionString(), sql.ToString()).ToString());

            sql = new SqlBuilder();
            sql.Append("select count(*) from phoenix.training_quizzes where training_course_id={0}", CourseId);  
            int totalQuizzes = int.Parse(SqlHelper.ExecuteScalar(Library.ConnectionString(), sql.ToString()).ToString());

            if (totalQuizzes != 0 && totalQuizzes == quizzesTaken)
            {
                sql = new SqlBuilder();
                sql.Append("UPDATE tblTrainingRegistration Set Attended=1 WHERE TrainingRegistrationId={0}", RegistrationId);
                SqlHelper.ExecuteNonQuery(Library.ConnectionString(), sql.ToString());
            }
        }
        private void SaveCourseRecord(int personId, int quizId, int score)
        {
            SqlBuilder sql = new SqlBuilder();
            string registrationId = "";
            try
            {
                //sql.Append("SELECT TrainingRegistrationId FROM tblTrainingRegistration");
                //sql.Append("WHERE ScheduleId={0} AND PersonId={1}", scheduleId, personId);
                //registrationId = SqlHelper.ExecuteScalar(Library.ConnectionString(), sql.ToString()).ToString();
                //logger.Debug("Found Registration ID: {0}", registrationId);
            }
            catch
            {
                //sql = new SqlBuilder();
                //sql.Append("INSERT INTO tblTrainingRegistration");
                //sql.Append("(PersonId, ScheduleId, TestScore, Attended)");
                //sql.Append("Values({0},{1},{2},1)", personId, scheduleId, score);
                //                sql.Append("Values({0},{1},{2},1)", personId, scheduleId, score);
                try
                {
                    SqlHelper.ExecuteNonQuery(Library.ConnectionString(), sql.ToString());
                    logger.Debug("successfully inserted");
                }
                catch (Exception ex)
                {
                    //logger.Debug("Insert Error - PersonId: {0}, ScheduleId:{1}; {2}", personId, scheduleId, ex.Message);
                }
            }
            if (registrationId != "")
            {
                try
                {
                    //sql.Append("Update tblTrainingRegistration");
                    //sql.Append("SET TestScore={0}, Attended=1", score);
                    //sql.Append("WHERE ScheduleId={0} AND PersonId={1}", scheduleId, personId);
                    //registrationId = SqlHelper.ExecuteScalar(Library.ConnectionString(), sql.ToString()).ToString();
                    //logger.Debug("Successfully updated");
                }
                catch (Exception ex)
                {
                    //logger.Debug("Update Error - PersonId: {0}, ScheduleId:{1}; {2}", personId, scheduleId, ex.Message);
                }

            }
        }
    }
}

