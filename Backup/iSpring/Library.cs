﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using NLog;
using NLog.Targets;
using NLog.Config;

namespace iSpring
{
    public static class Library
    {
        public static string ConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["acpbeConnectionString"].ToString();
        }

        public static void NLoggerSetup(ref Logger logger)
        {
            var fileTarget = new FileTarget();
            var config = new LoggingConfiguration();
            config.AddTarget("file", fileTarget);

//            fileTarget.FileName = "${basedir}/logs/${shortdate}_log.txt";
            fileTarget.FileName = "C:/Windows/Temp/ispring_${shortdate}_log.txt";
            fileTarget.Layout = @"${date} ${message}";


            var rule2 = new LoggingRule("*", LogLevel.Trace, fileTarget);
            config.LoggingRules.Add(rule2);
            LogManager.Configuration = config;
            logger = LogManager.GetLogger("Importer");
        }
    }
}